import java.util.Scanner;

public class Nod_for_four_numbers {
    public static void main(String[] args) {
       int a, b, c, d, nod;
       a = inputIntFromConsole("Enter the first value: ");
       b = inputIntFromConsole("Enter the second value: ");
       c = inputIntFromConsole("Enter the third value: ");
       d = inputIntFromConsole("Enter the fourth value: ");
       nod = nod(a, b, c, d);
       display(nod);
    }
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Incorrect value. " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int nod(int a, int b, int c, int d) {
        while (a != 0 && b != 0) {
            if (a > b) { a = a % b; }
            else { b = b % a; }
        }
        int nod1 = a + b;
        while (c != 0 && d != 0) {
            if (c > d) { c = c % d; }
            else { d = d % c; }
        }
        int nod2 = c + d;
        while (nod1 != 0 && nod2 != 0) {
            if (nod1 > nod2) { nod1 = nod1 % nod2; }
            else { nod2 = nod2 % nod1; }
        }
        return nod1 + nod2;
    }
    public static void display(int nod){
        System.out.println("The greatest common divisor: " + nod);
    }
}